@component('listings.partials._base_listing', compact('listing'))
    @slot('links')
        <ul class="list-inline">
            <li>Added {{ $listing->pivot->created_at->diffForHumans() }}</li>
            <li><a href="#" onclick="event.preventDefault(); document.getElementById('delete-favourite-{{ $listing->id }}').submit();">Delete</a></li>
        </ul>

        <form id="delete-favourite-{{ $listing->id }}" action="{{ route('listings.favourites.destroy', [$area, $listing]) }}" method="POST" style="display: none;">
            {{ csrf_field() }}
            {{ method_field('delete') }}
        </form>
    @endslot
@endcomponent