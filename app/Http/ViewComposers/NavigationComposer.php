<?php

namespace App\Http\ViewComposers;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;

class NavigationComposer
{
    public function compose(View $view)
    {
        if (!Auth::check()) {
            return $view;
        }

        $user = Auth::user();
        $listings = $user->listings;

        return $view->with([
            'unpublishedListingsCount' => $listings->where('live', false)->count(),
            'publishedListingsCount' => $listings->where('live', true)->count(),
        ]);
    }
}